from random import *


def list_to_string(data):
    my_string: str = ""
    for i in data:
        if type(i) == list:
            for ii in i:
                my_string += ii
            continue
        my_string += i
    return my_string


result = ""

items = [["L", "Z", "D", "K", "R"], ["3"], ["H", "C"], ["O"]]
limits = [15, 1, 2, 1]


total_limit = 20

# first add
result += list_to_string(items[0][randint(0, len(items[0]) - 1)])
rule = 2

while len(result) < 20:

    random_box_index = randint(0, len(items) - 1)
    random_box = items[random_box_index]

    random_char = random_box[randint(0, len(random_box) - 1)]

    if result[len(result) - 1] == random_char:
        continue

    if limits[random_box_index] == 0:
        continue

    elif random_char not in items[0]:
        if rule > 0:
            continue
        else:
            rule = 3
            result += random_char
            limits[random_box_index] -= 1

    else:
        if random_char in items[0]:
            rule -= 1

        result += random_char
        limits[random_box_index] -= 1

print(result)
print(len(result))

